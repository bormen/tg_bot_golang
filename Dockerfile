FROM golang:1.15.12-alpine3.13 AS builder

WORKDIR /bot
COPY . .

RUN apk add git && \
    mv ./go/src/* /go/src && \
#    go env -w GO111MODULE=off && \
    go get github.com/go-telegram-bot-api/telegram-bot-api && \
    go build -o bot


FROM alpine:latest
COPY --from=builder ./bot /bin/

CMD ["/bin/bot"]

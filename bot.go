package main

import (
	"log"
	"commandhandler"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"os"
)

func main() {
	bot, err := tgbotapi.NewBotAPI("1479232100:AAGcjUHwYQAXOZeNSUwdvHYy946EpSjDoqg")
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true


	file, err := os.OpenFile("/var/log/bot.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	    if err != nil {
	            log.Fatal(err)
	    }
	
	defer file.Close()
	log.SetOutput(file)
	
	
	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	
	updates := bot.GetUpdatesChan(u)
	
	for update := range updates {

		if update.Message == nil {
			continue
		}
		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		if update.Message.IsCommand() {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

			msg.Text = mainhandler.ProcessCommand(update.Message.Command())
			bot.Send(msg)
		}
	}
	
}

package flipcoin

import (
	"math/rand"
	"time"
)

func Throw() (Result string) {
	
	rand.Seed(time.Now().UnixNano())
	
	if rand.Intn(100) >= 50 {
	    Result = "Head"
	} else {
	    Result = "Tail"
	}
	return
}
